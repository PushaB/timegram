#!/usr/bin/env ruby

def hello(name)
	if name.nil?
		puts "Hello, World!"
	else
		puts "Hello, #{name}!"
	end
#Комментарий
end

if __FILE__ == $0
	hello(ARGV[0])
end
