class OnetimeToken < ApplicationRecord
  belongs_to :user
  before_create  :set_token

  def set_token(length = 6)
	  numbers = '0123456789'
	  a = ''
	  length.times {a << numbers[rand(numbers.size)]}
	  self.token = a
  end
end
