class CreateOnetimeTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :onetime_tokens do |t|
      t.references :user, foreign_key: true
      t.string :token, limit: 6

      t.timestamps
    end
  end
end
