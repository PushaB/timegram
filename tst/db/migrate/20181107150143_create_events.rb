class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      
      t.string :title, null: false
      t.timestamp :start_at, null: false
      t.timestamp :created_at, null: false
      t.text :description
      t.timestamp :end_at
#      t.timestamp :updated_at

      t.timestamps
    end
  end
end
