require 'json/ext'
class CounterController < ApplicationController
	def home
		if Counter.first.nil?
			Counter.create(count:1)
		end
		@cnt = Counter.first.count
		@greet = "Hello, World! #{@cnt} times"
		@greeting = {:result =>  @greet}.to_json
		#render json: [:result , @greet]
		Counter.increment_counter(:count, 1)
	end	
end
